from queue import Queue
from WorkerThread import Worker
import socket
import time
import threading

HOST = socket.gethostbyname(socket.gethostname())
PORT = 5050

WORKER_MAX = 3
JOB_MAX = 2

def make_worker():
    worker = Worker(Queue())
    worker.start()
    time.sleep(0.1)

    return worker

def get_worker(workers):
    # FITUR LOAD BALANCER
    queue_length = [x.queue.qsize() for x in workers]

    # FITUR UPSCALE
    # Jika semua worker memiliki 2 atau lebih queue
    # job, maka membuat worker baru
    if min(queue_length) > JOB_MAX : 
        workers.append(make_worker())
            
    return queue_length.index(min(queue_length))

def main():
    print("[SERVER START]")
    workers = [make_worker() for i in range(WORKER_MAX)]
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST,PORT))
            s.listen()
            print("[LISTENING]")
            conn, addr = s.accept()
            with conn:
                print(f"[CONNECTED] to {addr}")
                try:
                    while True:
                        data = conn.recv(8192)
                        if not data: break
                        job = data.decode()
                        choosen = get_worker(workers)
                        workers[choosen].queue.put((job, conn))
                except KeyboardInterrupt:
                    print("[SERVER STOP]")
                    break
                except:
                    print(f"[DISCONNECTED] by {addr}")

if __name__ == "__main__":
    main()

                        



    