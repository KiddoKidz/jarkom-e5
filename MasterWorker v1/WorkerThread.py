from queue import Queue
import socket
import time
import threading

STATUS_WAITING, STATUS_RUNNING, STATUS_FAILED, STATUS_DONE = 'WAITING', 'RUNNING', 'FAILED', 'DONE'

WORKER_ACTIVE, WORKER_DEAD, WORKER_RUNNING = 'ACTIVE', 'DEAD', 'RUNNING'

LOCK = threading.Lock()

class Worker(threading.Thread):
    def __init__(self, queue, args=(), kwargs=None):
        threading.Thread.__init__(self, args=(), kwargs=None)
        self.job_status = STATUS_WAITING
        self.worker_status = WORKER_ACTIVE
        self.queue = queue
        self.job = 0
        self.worker_name = ''

    def run(self):
        while True:
            if self.worker_status == WORKER_DEAD: break
            if (self.worker_name == ''):
                angka = threading.currentThread().getName()
                self.worker_name = "Worker-" + angka[-1]

            self.job_status = STATUS_WAITING
            data, conn = self.queue.get()
            self.job_status = STATUS_RUNNING
            self.worker_status = WORKER_RUNNING
            self.job += 1
            command = data.split(' ')[0]
            raww = data.split(' ')[1]
            
            print(f"{self.worker_name}: Job {self.job}, ({self.job_status})")

            try:
                if command == "1": # JOB: POWER (FAST)
                    ress = str(int(raww)**2)
                    raww = raww + "^2"
                    time.sleep(1)
                elif command == "2": # JOB: BUBBLESORT (SLOW)
                    dataString = raww.split(',')
                    ress = [int(i) for i in dataString]
                    n = len(ress) 
                    for i in range(n-1):  
                        for j in range(0, n-i-1):  
                            if ress[j] > ress[j+1] : 
                                ress[j], ress[j+1] = ress[j+1], ress[j]

                    time.sleep(3)
                self.send((raww, ress), conn)
                self.job_status = STATUS_DONE
            except Exception as e:
                print(e)
                self.job_status = STATUS_FAILED
            finally:
                print(f'{self.worker_name}: Job {self.job}, ({self.job_status})')

    def send(self, res, conn):
        LOCK.acquire()
        try:
            conn.send(f"{res[0]} = {res[1]}".encode())
        finally:
            LOCK.release()

