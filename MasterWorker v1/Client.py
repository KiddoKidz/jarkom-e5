import socket
import threading
import random

# IP address pada server harus selalu diganti setiap
# mengganti instance, menggunakan Public IP Address milik
# instance yang sudah dibuat.
SERVER = '3.238.123.46'
PORT = 5050

SEND_LOCK = threading.Lock()
BUFFER_LOCK = threading.Lock()
LISTEN_LOCK = threading.Lock()

# List untuk menyimpan message yang diterima
BUFFER = []

def listening_thread(sock):
    while True:
        data = sock.recv(1024)
        LISTEN_LOCK.acquire()
        BUFFER.append(data.decode())
        LISTEN_LOCK.release()

def print_buffer():
    BUFFER_LOCK.acquire()
    if len(BUFFER) == 0:
        print("No output available")
    else:
        print("\n".join(list(map(lambda x: f"{x}", BUFFER))))
    BUFFER.clear()
    BUFFER_LOCK.release()

def send_thread(sock, data):
    SEND_LOCK.acquire()
    sock.send(data.encode())
    SEND_LOCK.release()

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((SERVER, PORT))

        listen_thread = threading.Thread(target=listening_thread, args=(s,), daemon=True).start()

        print(f"{SERVER} Connected")
        print("Command available:\n1. power [number to be powered: int] (FAST)\n2. sort [array length: int(max: 1000)] (SLOW)\n4. exit\n5. output\n")

        while True:
            
            angka = ''
            data_input = input("> ")
            inputString = data_input.split(' ')
            command = inputString[0]
            if command == "power" or command == "sort":
                try:
                    if command == "power":
                        angka = '1 ' + inputString[1]
                    elif command == "sort":
                        generated_array = generate_array(int(inputString[1]))
                        data = array_to_string(generated_array)
                        angka = '2 ' + data
                    
                    x = threading.Thread(target=send_thread, args=(s, angka), daemon=True)
                    x.start()
                except ValueError:
                    print("No number input")

            elif command == "exit":
                print(f"{SERVER} Disconnected")
                break
            elif command == "output":
                print_buffer()
            else:
                print("Command not exist")

def generate_array(arr_length):
    if arr_length > 1000:
        arr_length = 1000
    generated_array = [i**2 for i in range(arr_length)]
    random.shuffle(generated_array)
    return generated_array

def array_to_string(array):
    converted_array = ','.join(map(str, array))
    return converted_array

if __name__ == "__main__":
    main()