import socket 
import uuid
import random
import json
import threading

class Client:
  def __init__(self):
    self.HOST = socket.gethostbyname(socket.gethostname())
    self.PORT = 50007

    self.RECEIVE_DATA_THREAD = None

    self.SOCKET = None

    self.JOB_CODE = {
      'SORTING': 'SJ',
      'POWER': 'PJ'
    }

    self.COMMAND_CODE = {
      0: ['OPEN CONNECTION', self.open_connection],
      1: ['CLOSE CONNECTION', self.close_connection],
      2: ['SORTING JOB', self.generate_sorting_job],
      3: ['POWER JOB', self.generate_power_job],
      4: ['SHOW JOBS & RESULTS', self.show_jobs_and_results],
      5: ['EXIT', self.exit_program],
    }

    self.SENT_JOBS = {}

    self.SEND_JOB_DATA = {
      'JOB_ID': None,
      'DATA': None,
    }

    self.USERNAME = ''
    self.USER_ID = 0

  def main(self):
    print('[CLIENT START]')
    self.USERNAME = input('Input your name : ')
    self.generate_user_id(self.USERNAME)
    while True:
      if input('Do you want to see command list? [y/n]') == 'y':
        self.show_commands()
        self.choose_command()

  def show_commands(self):
    print('\nHello {0}\nHere are the list of available commands:'.format(self.USERNAME))
    for (code, comm_list) in self.COMMAND_CODE.items():
      print(str(code) + ' : ' + comm_list[0])

  def generate_user_id(self, username):
    uuid_last_num = str(uuid.uuid4().fields[-1])
    self.USER_ID = username[:3] + '-' + uuid_last_num

  def choose_command(self):
    code = -1
    while True:
      try:
        code = int(input('\nChoose a command code [0/1/2/3/4/5] : '))
      except:
        print('[CHOOSE COMMAND] Chosen code must be integer!')

      if self.COMMAND_CODE.get(int(code)):
        break
      else:
        print('[CHOOSE COMMAND] Command code is not available! Please choose the right code!')

    if code == 0 or code == 1 or code == 5 or code == 4:
      self.COMMAND_CODE[code][1]()
    else:
      if self.SOCKET:
        generated_job_data = self.COMMAND_CODE[code][1]()
        self.send_job(generated_job_data)
      else:
        print('Socket must be opened first!')


  def generate_sorting_job(self):
    while True:
      try:
        arr_length = int(input('Input the array size [max : 10000]: '))
        if arr_length > 10000:
          arr_length = 10000
        break
      except:
        print('[SORTING JOB] Array length must be an integer')
    
    generated_array = [i**2/2 for i in range(arr_length)]
    random.shuffle(generated_array)
    print('It is generated array for you :\n{0}'.format(generated_array))

    uuid_last_num = str(uuid.uuid4().fields[-1])
    
    job_id = '{0}-{1}-{2}'.format(self.JOB_CODE['SORTING'], uuid_last_num, self.USER_ID)

    job_data = {
      'JOB_ID' : job_id,
      'DATA' : generated_array
    }

    return job_data

  
  def send_job(self, job_data):
    send_job_data = self.SEND_JOB_DATA.copy()
    send_job_data['JOB_ID'] = job_data['JOB_ID']
    send_job_data['DATA'] = job_data['DATA']
    encoded_data = self.encode_and_stringify_data(send_job_data)
    if self.SOCKET:
      self.SOCKET.sendall(encoded_data)
      self.SENT_JOBS[send_job_data['JOB_ID']] = { 
        'DATA': send_job_data['DATA'],
        'RESULT' : ''
      }
    else:
      print('[SEND JOB] Socket closed!')

  def receiving_data(self):
    print('[RECEIVING DATA]')
    while True:
      try:
        received_data = self.SOCKET.recv(4096)
        if received_data:
          print(received_data)
          loaded_json = self.decode_and_load_json_data(received_data)
          print(loaded_json)
          self.SENT_JOBS[loaded_json['JOB_ID']]['RESULT'] = loaded_json['RESULT']
      except:
        print('[RECEIVE] Socket closed')
        break
      
  def encode_and_stringify_data(self, data):
    json_string = json.dumps(data)
    encoded_json_string = json_string.encode('utf-8')
    return encoded_json_string

  def decode_and_load_json_data(self, data):
    decoded_string = data.decode('utf-8')
    loaded_json = json.loads(decoded_string)

    return loaded_json

  def generate_power_job(self):
    while True:
      try:
        number = int(input('Input the number for x^2 operation : '))
        break
      except:
        print('[POWER JOB] The number should be integer! Try Again!')

    uuid_last_num = str(uuid.uuid4().fields[-1])
    
    job_id = '{0}-{1}-{2}'.format(self.JOB_CODE['POWER'], uuid_last_num, self.USER_ID)

    job_data = {
      'JOB_ID' : job_id,
      'DATA' : number
    }

    return job_data
  
  def open_connection(self):
    if not self.SOCKET:
      try:
        self.SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.SOCKET.connect((self.HOST, self.PORT))
        self.RECEIVE_DATA_THREAD = threading.Thread(target=self.receiving_data)
        self.RECEIVE_DATA_THREAD.start()
        print('[OPEN CONNECTION] Open connection succeed')
      except:
        print('[OPEN CONNECTION] Open connection failed!')


  def close_connection(self):
    if self.SOCKET:
      self.SOCKET.shutdown(1)
      self.SOCKET.close()
      print('[CLOSE CONNECTION] Socket connection closed!')
      self.RECEIVE_DATA_THREAD.join()
      self.SOCKET = None
    else:
      print('[CLOSE CONNECTION] No socket connection opened!')

  def show_jobs_and_results(self):
    if self.SENT_JOBS:
      for (job_id, job_detail) in self.SENT_JOBS.items():
        print('[{0}]\nDATA : {1}\nRESULT : {2}\n'.format(job_id, job_detail['DATA'], job_detail['RESULT']))

  def exit_program(self):
    self.close_connection()
    exit()

if __name__ == "__main__":
  client = Client()
  client.main()