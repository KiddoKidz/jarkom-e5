import socket
import json
import threading

class Master:

  def __init__(self):
    self.LISTEN_CLIENT_SOCKET = socket.socket() 
    self.CONNECT_WORKERS_SOCKET = []

    self.CLIENT_DONE_JOB_LIST_COND = threading.Condition()
    self.CLIENT_HOLD_JOB_LIST_COND = threading.Condition()

    self.CLIENT_DONE_JOB_LIST = {}
    self.CLIENT_HOLD_JOB_LIST = {}

    self.HOST = socket.gethostbyname(socket.gethostname()) 
    self.CLIENT_PORT = 50007
    self.WORKERS_PORT = [60001,  60002, 60003]

    self.THREADS = {
      'LISTEN_THREAD': None,
      'WORKER_THREAD': [],
      'HANDLER_CLIENT_THREADS':[],
      'RECEIVER_CLIENT_THREADS':[],
      'SENDER_CLIENT_THREADS':[]
    }

  def main(self):
    print('[SERVER START]')
    print('[START LISTEN THREAD]')
    self.THREADS['LISTEN_THREAD'] = [threading.Thread(target=self.listening, args=(self.HOST, self.CLIENT_PORT)).start()]
    # self.listening(self.HOST, self.CLIENT_PORT)
    # print['[START WORKERS THREAD]']
    # self.THREADS['WORKERS_THREAD'] = [threading.Thread(target=self.connect_worker, args=(self.HOST, self.WORKERS_PORT[i])) for i in range(3)]

  def listening(self, host, port):
    print('[LISTENING] HOST : {0} , PORT {1}'.format(host, port))
    self.LISTEN_CLIENT_SOCKET.bind((host, port))
    self.LISTEN_CLIENT_SOCKET.listen()
    while True:
      try:
        conn, addr = self.LISTEN_CLIENT_SOCKET.accept()
        self.THREADS['HANDLER_CLIENT_THREADS'].append(threading.Thread(target=self.handle_client, args=(conn, addr)).start())
      except KeyboardInterrupt:
        self.exit_program()

  def exit_program(self):
    for thread in self.THREADS['RECEIVER_CLIENT_THREADS']:
      thread.join()
    for thread in self.THREADS['HANDLER_CLIENT_THREADS']:
      thread.join()
    for thread in self.THREADS['WORKER_THREADS']:
      thread.join()
    self.THREADS['LISTEN_THREAD'].join()

  def handle_client(self, conn, addr):
    self.CLIENT_HOLD_JOB_LIST[addr[1]] = []
    self.CLIENT_DONE_JOB_LIST[addr[1]] = []
    print('[CONNECTED] {0}'.format(addr))
    self.THREADS['RECEIVER_CLIENT_THREADS'].append(threading.Thread(target=self.recieve_from_client, args=(conn, addr)).start())
    self.THREADS['SENDER_CLIENT_THREADS'].append(threading.Thread(target=self.send_to_client, args=(conn, addr)).start())

  def send_to_client(self, conn, addr):
    with conn:
      self.CLIENT_DONE_JOB_LIST_COND.acquire()
      while True:
        try:
          for i in range(len(self.CLIENT_DONE_JOB_LIST[addr[1]])):
            print('[SEND CLIENT]')
            data_job_done = self.CLIENT_DONE_JOB_LIST[addr[1]].pop(0)
            encoded_data = self.encode_and_stringify_data(data_job_done)
            # print(encoded_data)
            conn.sendall(encoded_data)
          self.CLIENT_DONE_JOB_LIST_COND.wait()
        except:
          break
      self.CLIENT_DONE_JOB_LIST_COND.release()

  def recieve_from_client(self, conn, addr):
    with conn:
      while True:
        try :
          data = conn.recv(4096)
          if not bool(data):
            break
          json_data = self.decode_and_load_json_client(data)
          self.CLIENT_HOLD_JOB_LIST_COND.acquire()
          self.CLIENT_HOLD_JOB_LIST[addr[1]].append(json_data)
          self.CLIENT_HOLD_JOB_LIST_COND.notify()
          self.CLIENT_HOLD_JOB_LIST_COND.release()

          #LAKUIN SORT DI WORKER, TERUS BALIKIN LAGI HASIL SORTINGNYA, TERUS MASUKIN KE LIST DONE_JOB
          json_data['RESULT'] = sorted(json_data['DATA'])
          self.CLIENT_DONE_JOB_LIST_COND.acquire()
          self.CLIENT_DONE_JOB_LIST[addr[1]].append(json_data)
          self.CLIENT_DONE_JOB_LIST_COND.notify()
          self.CLIENT_DONE_JOB_LIST_COND.release()
          # print(self.CLIENT_DONE_JOB_LIST)
        except:
          break
      print('[DISCONNECTED] {0}'.format(addr))

  def encode_and_stringify_data(self, data):
    json_string = json.dumps(data)
    encoded_json_string = json_string.encode('utf-8')
    return encoded_json_string
  
  def decode_and_load_json_client(self, data):
    decoded_string = data.decode('utf-8')
    json_loaded = json.loads(decoded_string)
    return json_loaded
  # def connect_worker(self, host, port): 


if __name__ == "__main__":
    master = Master()
    master.main()